///*
// * This file contains some method useful when you 	
// * If you don't know what it is or you simply don't need it, just remove this file
// * */
//
//package ginlemon.library;
//
//
//import android.content.ActivityNotFoundException;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//
//public class helper {
//
//	/** 
//	 * Check if a SmartLauncher version is installed. 
//	 * **/
//	boolean isSmartLauncherInstalled(Context context){
//		String[] packagenames = {"ginlemon.flowerfree", "ginlemon.flowerpro","ginlemon.flowerpro.special"};
//		for(String pname : packagenames){
//			Intent i = new Intent();
//			i.setPackage(pname);
//			PackageManager pm = context.getPackageManager();
//			if(pm.queryIntentActivities(i, 0).size()>0){
//				return true;
//			};
//		}
//		return false;
//	}
//
//	/**
//	 * Set this theme in SmartLauncher
//	 * **/
//	public static void setTheme(Context context) throws ActivityNotFoundException{
//		context.startActivity(new Intent().setAction("ginlemon.smartlauncher.setGSLTHEME").putExtra("package", context.getPackageName()));
//	}
//
//}
